extends RigidBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var mouse_sens = 0.1
var angle = 0
var camera: Camera
var peerid = 0
var speed: Vector3 = Vector3(30,30,30)
var speedLimit: Vector3 = Vector3(50,50,50)
var dir: Vector3 = Vector3(0,0,0)
var timer
var mutex
var semaphore
var thread
var exit_thread = false
var lastPos: Vector3
var jdur = 1.0
var actual_jump_dur = 0.0
var jumpdelay = 3.0
var actual_jump_delay = 0.0
var jumping = false
var stop = 0.0
var stopval = 0.017
var netnode
var smashing = false
var smash_delay = 0.3
var actual_smash_delay = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	self.camera = get_node("CSGBox2/Camera")
	mutex = Mutex.new()
	semaphore = Semaphore.new()
	exit_thread = false

	thread = Thread.new()
	thread.start(self, "send_data",[],1)
	
remote func move_player_resp():
	pass

func players_position():
	pass

remote func rotate_player(radsx, radsy, id):
	var player = self
	if player.peerid == id:
		if radsx + player.angle < 90 and radsx + player.angle > -90:
			player.camera.rotate_x(deg2rad(radsx))
			player.angle += radsx
		player.rotate_y(radsy)

func _process(delta):
	# Make the thread process.
	
	stop += delta
	if self.netnode.peer.get_connection_status() and stop >= stopval:
		stop = 0
		semaphore.post()
	if self.transform.origin.y <= -30:
		self.transform.origin.x = 2
		self.transform.origin.y = 2
		self.transform.origin.z = 2
func magnitude(a: Vector3):
	return sqrt(abs(a.x)+abs(a.y)+abs(a.z))
	
remote func move_player(dir):
	self.dir = dir

remote func player_smashing():
#	print("smashing incoming")
	self.smashing = true

remote func player_stop_smashing():
#	print("smashing stop")
	self.smashing = false
	
func _physics_process(delta):
	var force = dir * self.speed * delta * 100
	if magnitude(self.linear_velocity) > 5:
		return
	else:
		self.add_central_force(force)
		lock_jump(delta)
		lock_smash(delta)
func lock_jump( delta: float):
	
	if self.dir.y > 0:
		actual_jump_dur = actual_jump_dur + delta
		if actual_jump_dur >= jdur and actual_jump_delay < 0:
			actual_jump_delay = jumpdelay
			self.dir.y = -10.0
		if actual_jump_delay >= 0:
			self.dir.y = -10.0
	else:

		actual_jump_delay -= delta
		if actual_jump_delay <= 0:
			actual_jump_dur = 0.0
			self.dir.y = 0

func lock_smash( delta: float):
	
	if self.smashing:
		if actual_smash_delay <= 0:
			actual_smash_delay = smash_delay
			self.dir.y = -100.0
	else:
		actual_smash_delay -= delta
	

func send_data(data):
	while true:
		semaphore.wait() # Wait until posted.
		mutex.lock()
		var should_exit = exit_thread # Protect with Mutex.
		mutex.unlock()

		if should_exit:
			break
		mutex.lock()
		var rotc = self.camera.get_rotation()
		var pos = self.transform.origin
		var rot = self.get_rotation()
		var lpos = self.lastPos
		mutex.unlock()
		rpc_unreliable("move_player_resp", pos, rot, rotc)
		mutex.lock()
		self.lastPos = pos
		mutex.unlock()

func _exit_tree():
	# Set exit condition to true.
	mutex.lock()
	exit_thread = true # Protect with Mutex.
	mutex.unlock()

	# Unblock by posting.
	semaphore.post()

	# Wait until it exits.
	thread.wait_to_finish()

func get_player_position():
	pass
