extends Spatial

var peer = NetworkedMultiplayerENet.new()

var SERVER_PORT = 8082
var MAX_PLAYERS = 13
var timer: Timer
var players = {}

# Connect all functions
func _ready():
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().network_peer = peer
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

remote func register_player(id, pos, op=false):
	var player = preload("res://player.tscn").instance()
	players[str(id)] = player
	player.transform.origin = pos
	player.set_network_master(id,true)
	player.netnode = self
	player.peerid = id
	peer.get_connection_status()
	self.add_child(player)
	

func _player_connected(id):
	var pos = Vector3(
		randf()*40-20,
		randf()*40-20,
		randf()*40-20
	)
	
	for p in players.keys():
		rpc_id(int(p),"register_player",players[str(p)].peerid,players[str(p)].transform.origin, true)
	for p in players.keys():
		rpc_id(id,"register_player",players[str(p)].peerid,players[str(p)].transform.origin, true)
	
	rpc("register_player", id, pos, false)
	register_player(id,pos,false)
	
	
func _player_disconnected(id):
	players.erase(id) # Erase player from info.

func _connected_ok():
	pass # Only called on clients, not server. Will go unused; not useful here.

func _server_disconnected():
	pass # Server kicked us; show error and abort.

func _connected_fail():
	pass # Could not even connect to server; abort.
